SOURCES += \
    $$PWD/dvideowidget.cpp \
    $$PWD/hotzone.cpp \
    $$PWD/hoverwidget.cpp \
    $$PWD/dbus/dbuszone.cpp \
    $$PWD/pushbuttonlist.cpp \
    $$PWD/mainwindow.cpp

HEADERS  += \
    $$PWD/dvideowidget.h \
    $$PWD/hotzone.h \
    $$PWD/hoverwidget.h \
    $$PWD/dbus/dbuszone.h \
    $$PWD/pushbuttonlist.h \
    $$PWD/mainwindow.h

RESOURCES += \
    $$PWD/image.qrc

